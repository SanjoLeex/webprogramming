import { Component, OnInit } from '@angular/core';
import * as proj from './projects.json';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  projectTitle: string;
  projectCode: string;

  constructor() { }

  ngOnInit() {
    
    this.projectTitle = proj.title;
    this.projectCode = proj.code;
    console.log(this.projectTitle);
    document.getElementById("title").innerHTML = this.projectTitle;
    document.getElementById("code").innerHTML = this.projectCode;
  }

}
