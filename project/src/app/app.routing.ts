import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { AboutComponent } from './about';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'about', component: AboutComponent },

    { path: '**', redirectTo: '' }
]

export const appRoutingModule = RouterModule.forRoot(routes);